﻿using System.Collections;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    #region Editor Params
    public Transform anchor;
    public BoxCollider boxCol;
    public Node firstNode;
    public float horiSpeed;
    public float vertSpeed;
    public float sizeY;
    public float nextStepLength;
    public float nextStepHeight;
    public Transform standingStep;
    public Transform trail;
    #endregion

    #region Params
    public static Player Instance { get; private set; }
    public Node rootNode;
    private Vector3 lastFrameTouchPos;
    private Vector3 deltaTouchPos;
    private Vector3 tempPos;
    private Vector3 tempRootPos;
    private Vector3 tempAnchorLocalPos;
    private int frameHold;
    public List<Node> listNode = new List<Node>();
    public Transform currentSetStanding;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        Initialize();
    }

    public void Initialize()
    {
        rootNode = firstNode;
        tempPos = transform.position;
        tempRootPos = rootNode.transform.position;
        tempAnchorLocalPos = anchor.localPosition;

        listNode.Add(firstNode);
        SetAnimation(rootNode.currentAnimator, "Run");
    }

    void Update()
    {
        MoveVertical();
        if (!IsPointerOverUIObject())
        {
            if (Input.GetMouseButton(0))
            {
                deltaTouchPos = Input.mousePosition - lastFrameTouchPos;
                lastFrameTouchPos = Input.mousePosition;
                if (frameHold == 0)
                {
                    deltaTouchPos = Vector3.zero;
                    frameHold = 1;
                    return;
                }
                MoveHorizontal(deltaTouchPos);
            }
            else
            {
                tempPos = transform.position;
                deltaTouchPos = Vector3.zero;
                lastFrameTouchPos = Vector3.zero;
                frameHold = 0;
            }
        }
    }

    public void MoveVertical()
    {
        tempPos.z += Time.deltaTime * vertSpeed;
        transform.position = tempPos;
        tempRootPos = rootNode.transform.position;
        trail.localPosition = rootNode.transform.localPosition;
    }

    public void MoveHorizontal(Vector3 deltaPos)
    {
        tempPos.x += deltaPos.x * 0.01f * horiSpeed;
        tempPos.x = Mathf.Clamp(tempPos.x, -6.5f, 6.5f);
        transform.position = tempPos;
        tempAnchorLocalPos.x += deltaPos.x * 0.01f * horiSpeed;
        tempAnchorLocalPos.x = Mathf.Clamp(tempAnchorLocalPos.x, -0.75f, 0.75f);
        anchor.localPosition = tempAnchorLocalPos;
        UpdateBoxCollider();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Node") && !other.gameObject.GetComponent<Node>().isFirstNode)
        {
            StackUp(other.gameObject.GetComponent<Node>());
        }

        if (other.gameObject.CompareTag("Obstacle"))
        {
            if (rootNode.isFirstNode)
            {
                GameManager.Instance.LoseGame();
                Player.Instance.vertSpeed = 0f;
            }
            else
            {
                Obstacle temp = other.gameObject.GetComponent<Obstacle>();
                for (int i = 0; i < temp.value; i++)
                {
                    StackDown();
                }
            }
        }
        if (other.gameObject.CompareTag("Wall"))
        {
            standingStep = other.transform.parent.parent;
        }
        if (other.gameObject.CompareTag("Finish"))
        {
            GameManager.Instance.WinGame();
            RotatePlayer();
            Player.Instance.vertSpeed = 0f;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Bound"))
        {
            vertSpeed++;
            vertSpeed = Mathf.Clamp(vertSpeed, 12f, 25f);
            float percentAmountNodes;
            float percentListCount;

            percentAmountNodes = (int)(DataManager.Instance.currentLevel / 5f) * 0.1f;
            percentListCount = (int)(DataManager.Instance.currentLevel / 5f) * 0.1f;

            percentAmountNodes = Mathf.Clamp(percentAmountNodes, 0.175f, 0.35f);
            percentListCount = Mathf.Clamp(percentListCount, 0.125f, 0.25f);
            StepManager.Instance.SpawnNextStep((int)(StepManager.Instance.previousStep.amountNodes * percentAmountNodes) + (int)(listNode.Count * percentListCount), Random.Range(50f, 70f));
            other.transform.parent.parent.GetComponent<Step>().DelayReclaim();
        }
    }

    private void UpdateBoxCollider()
    {
        boxCol.center = new Vector3(rootNode.transform.localPosition.x, listNode.Count * sizeY * 0.5f + rootNode.transform.localPosition.y, 0f);
        boxCol.size = new Vector3(1f, listNode.Count * sizeY, 1f);
    }


    void StackUp(Node node)
    {
        MMVibrationManager.VibrateLight();
        tempRootPos.y = standingStep.localPosition.y + 1.5f + 0.3f;

        rootNode.transform.position = tempRootPos;
        SetAnimation(rootNode.currentAnimator, rootNode.startAnim[(int)Random.Range(0, rootNode.startAnim.Count)]);

        node.transform.SetParent(transform);
        node.transform.position = new Vector3(rootNode.transform.position.x, rootNode.transform.position.y - sizeY, rootNode.transform.position.z);
        node.tag = "Untagged";
        node.ConnectBody(anchor.GetComponent<Rigidbody>(), 0f);
        rootNode.ConnectBody(node.GetComponent<Rigidbody>(), 0.3f);
        rootNode = node;

        SetAnimation(rootNode.currentAnimator, "Run");

        listNode.Add(node);
        CorrectRotation();

        UpdateBoxCollider();
    }

    public void StackDown(Node nodeOut, float newY)
    {
        listNode.Remove(nodeOut);
        rootNode = listNode[listNode.Count - 1];
        rootNode.transform.localPosition = new Vector3(rootNode.transform.localPosition.x, newY + 0.3f, rootNode.transform.localPosition.z);
        rootNode.ConnectBody(anchor.GetComponent<Rigidbody>(), 0.4f);
        SetAnimation(rootNode.currentAnimator, "Run");
        CorrectRotation();
        UpdateBoxCollider();
    }

    [NaughtyAttributes.Button]
    public void StackDown()
    {
        if (listNode.Count > 0)
        {
            Node nodeOut = rootNode;
            listNode.Remove(nodeOut);
            Destroy(nodeOut.GetComponent<FixedJoint>());
            nodeOut.transform.SetParent(transform.parent);
            nodeOut.gameObject.SetActive(false);
            rootNode = listNode[listNode.Count - 1];
            rootNode.transform.localPosition = new Vector3(rootNode.transform.localPosition.x, standingStep.localPosition.y + 0.25f, rootNode.transform.localPosition.z);
            rootNode.ConnectBody(anchor.GetComponent<Rigidbody>(), 0.2f);
            SetAnimation(rootNode.currentAnimator, "Run");
            CorrectRotation();
            UpdateBoxCollider();
        }
    }

    void CorrectRotation()
    {
        foreach (Node node in listNode)
        {
            node.transform.localRotation = Quaternion.identity;
            node.transform.localPosition = new Vector3(0f, node.transform.localPosition.y, 0f);
        }
    }

    public void SetAnimation(Animator anim, string animString)
    {
        if (anim == null)
        {
            return;
        }
        anim.SetTrigger(animString);
    }

    public void RotatePlayer()
    {
        StartCoroutine(C_RotatePlayer());
    }

    IEnumerator C_RotatePlayer()
    {
        Quaternion temp = transform.localRotation;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 0.5f;
            transform.localRotation = Quaternion.Lerp(temp, Quaternion.Euler(0f, 180f, 0f), t);
            yield return null;
        }
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);

        if (Application.isEditor)
        {
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        else
        if (Application.isMobilePlatform)
        {
            eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        }

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    #region DEBUG
    #endregion

    #endregion
}

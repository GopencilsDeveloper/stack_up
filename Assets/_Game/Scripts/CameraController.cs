﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float lerpSpeed;
    public Player player;
    private Vector3 offset;
    private Vector3 startPos;

    void Start()
    {
        offset = transform.position - player.transform.position;
        startPos = transform.position;
        UpdateFOV();
    }

    void Update()
    {
        if (GameManager.Instance.currentState != GameState.WINGAME && GameManager.Instance.currentState != GameState.LOSEGAME)
        {


            float interpolation = lerpSpeed * Time.deltaTime;
            startPos.z = player.rootNode.transform.position.z + offset.z;
            startPos.y = Mathf.Lerp(startPos.y, player.rootNode.transform.position.y + offset.y, interpolation);
            transform.position = startPos;
        }
    }

    public void UpdateFOV()
    {
        float screenRatio = Camera.main.aspect;
        if (screenRatio <= 0.475f)  //9:19
        {
            Camera.main.fieldOfView = 60f;
        }
        else
        if (screenRatio <= 0.565f)  //9:16
        {
            Camera.main.fieldOfView = 55f;
        }
        else
        if (screenRatio <= 0.75)  //3:4
        {
            Camera.main.fieldOfView = 40f;
        }
    }
}

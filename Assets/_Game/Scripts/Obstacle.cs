﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    #region Editor Params
    public Rigidbody rigid;
    #endregion

    #region Params
    public int value;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods


    public void RandomSize()
    {
        transform.localScale = Vector3.one * Random.Range(0.35f, 0.6f);
    }

    public void SetSize()
    {
        transform.localScale = Vector3.one * value * 0.35f;
    }

    public void Move(float delay)
    {
        StartCoroutine(C_Move(delay));
    }

    IEnumerator C_Move(float delay)
    {
        yield return new WaitForSeconds(delay);
        rigid.velocity = Vector3.zero;
        rigid.velocity = Vector3.back * Random.Range(5f, 15f);
    }


    private void Update()
    {
        if (transform.position.y <= -40f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
    #endregion
}

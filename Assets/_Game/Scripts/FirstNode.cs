﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstNode : MonoBehaviour
{
    #region Editor Params
    public List<GameObject> listModel;
    #endregion

    #region Params
    [HideInInspector] public Player parentPlayer;
    public GameObject currentModel;
    #endregion


    private void OnEnable()
    {
        Initialize();
    }

    public void Initialize()
    {
        RandomModel();
    }

    public void RandomModel()
    {
        currentModel = null;
        foreach (GameObject item in listModel)
        {
            item.SetActive(false);
        }
        var m = listModel[(int)Random.Range(0, listModel.Count)];
        m.SetActive(true);
        currentModel = m;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            gameObject.SetActive(false);
        }
    }
}

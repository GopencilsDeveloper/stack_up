// algorithm referenced from http://www.gamasutra.com/view/feature/1648/random_scattering_creating_.php?print=1
// check out the video of it in action: https://youtu.be/iKjvFp5qa7A

using UnityEngine;
using System.Collections;

public class GridSpawner : MonoBehaviour
{
    public Transform spawnTransform;
    public float minSpacing;
    public float spacing;
    public int numColumns;
    public int numRows;

    public void Generate()
    {
        foreach (Transform obj in transform)
        {
            Destroy(obj.gameObject);
        }

        float radius = spacing * numColumns * 0.5f;

        Vector3 center = new Vector3(radius, 0f, radius);
        if (spawnTransform != null)
        {
            for (int i = 0; i < numColumns; i++)
            {
                for (int p = 0; p < numRows; p++)
                {
                    Vector3 wantedPosition = new Vector3(i * spacing, 0f, p * spacing);
                    if (Vector3.Distance(wantedPosition, center) <= radius)
                    {
                        float offsetX = (Random.value * 2f - 1f) * (spacing - minSpacing) * 0.5f;
                        float offsetY = (Random.value * 2f - 1f) * (spacing - minSpacing) * 0.5f;
                        Vector3 randomOffset = new Vector3(offsetX, 0f, offsetY);
                        Transform t = Instantiate(spawnTransform, wantedPosition + randomOffset, Quaternion.identity) as Transform;
                        t.parent = transform;
                    }
                }
            }
        }
    }
}

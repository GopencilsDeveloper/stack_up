﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstStep : MonoBehaviour
{
    public Step step;

    private void Start()
    {
        step.RandomNodesPosition((int)Random.Range(3f, 5f));
        step.SpawnNode(1, 1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebug : MonoBehaviour
{
    public Text txtLevel;
    public Text txtLose;
    public Text txtWin;

    private void Update()
    {
        UpdateLevel();
    }

    public void UpdateLevel()
    {
        txtLevel.text = DataManager.Instance.currentLevel.ToString();
    }

    public void Win()
    {
        txtWin.gameObject.SetActive(true);
        txtLose.gameObject.SetActive(false);
    }
    public void Lose()
    {
        txtLose.gameObject.SetActive(true);
        txtWin.gameObject.SetActive(false);
    }
}

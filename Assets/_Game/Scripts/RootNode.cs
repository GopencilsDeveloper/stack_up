﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootNode : MonoBehaviour
{
    #region Editor Params
    public Rigidbody rigid;
    #endregion

    #region Params
    private Vector3 tempPos;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Start()
    {
        tempPos = transform.position;
    }

    private void Update()
    {
    }

    public void MoveVertical()
    {
        tempPos.z += Time.deltaTime * 5f;
        transform.position = tempPos;
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingpongObject : MonoBehaviour
{
    public float start, end;
    public float minDuration, maxDuration;
    public float delay;
    Vector3 tempPos;

    private void OnEnable()
    {
        tempPos = transform.localPosition;
    }

    private void Start()
    {
        Waggle(start, end, minDuration, maxDuration, delay, true);
    }

    public void Waggle(float start, float end, float minDuration, float maxDuration, float delay = 0f, bool loopWaggle = true)
    {
        StartCoroutine(C_Waggle(start, end, Random.Range(minDuration, maxDuration), Random.Range(0f, 3f)));
    }

    IEnumerator C_Waggle(float start, float end, float duration, float delay = 0f, bool loopWaggle = true)
    {
        yield return new WaitForSeconds(delay);
        float t = 0f;
        while (t >= 0f && loopWaggle)
        {
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPos.z = Mathf.Lerp(start, end, Mathf.SmoothStep(0f, 1f, t));
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, tempPos.z);
                yield return null;
            }
            t = 0f;
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPos.x = Mathf.Lerp(end, start, Mathf.SmoothStep(0f, 1f, t));
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, tempPos.z);
                yield return null;
            }
            t = 0f;
        }
    }
}

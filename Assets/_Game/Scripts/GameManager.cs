﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    public UIDebug UIDebug;
    #endregion

    #region Params
    private static GameManager instance;
    public GameState currentState;
    #endregion

    #region Properties
    public static GameManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Application.targetFrameRate = 300;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
    }

    public void PlayGame()
    {
        ChangeState(GameState.INGAME);
    }

    public void RestartGame()
    {
        ChangeState(GameState.INGAME);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        print("RestartGame");
    }

    public void WinGame()
    {
        print("WinGame");
        ChangeState(GameState.WINGAME);
        DataManager.Instance.currentLevel++;
        DataManager.Instance.SaveData();
        UIDebug.UpdateLevel();
        UIDebug.Win();
    }

    public void LoseGame()
    {
        print("LoseGame");
        ChangeState(GameState.LOSEGAME);
        DataManager.Instance.SaveData();
        UIDebug.UpdateLevel();
        UIDebug.Lose();
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}

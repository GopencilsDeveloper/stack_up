﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour
{
    Vector3 tempPos;

    private void OnEnable()
    {
        tempPos = transform.localPosition;
        tempPos = new Vector3(Random.Range(-40, 60f), transform.localPosition.y, 190f);
        transform.localPosition = tempPos;
    }

    private void Start()
    {
        Waggle();
    }

    [NaughtyAttributes.Button]
    public void Waggle()
    {
        StartCoroutine(C_Waggle(Random.Range(-25f, 0f), 100f, 60f));
    }

    IEnumerator C_Waggle(float start, float end, float duration, float delay = 0f, bool loopWaggle = true)
    {
        yield return new WaitForSeconds(delay);
        float t = 0f;
        while (t >= 0f && loopWaggle)
        {
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPos.y = Mathf.Lerp(start, end, Mathf.SmoothStep(0f, 1f, t));
                transform.localPosition = new Vector3(transform.localPosition.x, tempPos.y, transform.localPosition.z);
                yield return null;
            }
            t = 0f;
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPos.y = Mathf.Lerp(end, start, Mathf.SmoothStep(0f, 1f, t));
                transform.localPosition = new Vector3(transform.localPosition.x, tempPos.y, transform.localPosition.z);
                yield return null;
            }
            t = 0f;
        }
    }
}

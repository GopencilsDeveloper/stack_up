﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region CONST
    public const string LEVEL = "LEVEL";
    public const string SCORE = "SCORE";
    public const string COIN = "COIN";
    #endregion

    #region Editor Params
    #endregion

    #region Params
    public int currentLevel;
    public int currentScore;
    public int currentCoin;
    #endregion

    #region Properties
    public static DataManager Instance { get; private set; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
        LoadData();
    }


    public void LoadData()
    {
        currentLevel = PlayerPrefs.GetInt(LEVEL);
        currentScore = PlayerPrefs.GetInt(SCORE);
        currentCoin = PlayerPrefs.GetInt(COIN);
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(LEVEL, currentLevel);
        PlayerPrefs.SetInt(SCORE, currentScore);
        PlayerPrefs.SetInt(COIN, currentCoin);
    }

    public void ResetData()
    {
        PlayerPrefs.SetInt(LEVEL, 0);
        PlayerPrefs.SetInt(SCORE, 0);
        PlayerPrefs.SetInt(COIN, 0);
    }

    #endregion
}

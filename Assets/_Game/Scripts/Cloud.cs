﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    Vector3 tempPos;

    private void OnEnable()
    {
        tempPos = transform.localPosition;
        tempPos = new Vector3(tempPos.x + Random.Range(-5f, 5f), tempPos.y + Random.Range(-5f, 5f), tempPos.z + Random.Range(-5f, 5f));
        transform.localPosition = tempPos;
    }

    private void Start()
    {
        Waggle();
    }

    [NaughtyAttributes.Button]
    public void Waggle()
    {
        StartCoroutine(C_Waggle(0f, 150f, Random.Range(120f, 175f), Random.Range(0f, 3f)));
    }

    IEnumerator C_Waggle(float start, float end, float duration, float delay = 0f, bool loopWaggle = true)
    {
        yield return new WaitForSeconds(delay);
        float t = 0f;
        while (t >= 0f && loopWaggle)
        {
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPos.x = Mathf.Lerp(start, end, Mathf.SmoothStep(0f, 1f, t));
                transform.localPosition = new Vector3(tempPos.x, transform.localPosition.y, transform.localPosition.z);
                yield return null;
            }
            t = 0f;
            while (t < 1f)
            {
                t += Time.deltaTime / (duration / 2f);
                tempPos.x = Mathf.Lerp(end, start, Mathf.SmoothStep(0f, 1f, t));
                transform.localPosition = new Vector3(tempPos.x, transform.localPosition.y, transform.localPosition.z);
                yield return null;
            }
            t = 0f;
        }
    }
}

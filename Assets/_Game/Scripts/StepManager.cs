﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepManager : MonoBehaviour
{
    #region Editor Params
    public Transform stepsParent;
    public Step stepPrefab;
    public Step previousStep;
    #endregion

    #region Params
    private static StepManager instance;
    List<Step> poolSteps = new List<Step>();
    Vector3 nextStepPos;
    public float heightStep;
    public int totalStepsAmount;
    public int currentStepsAmount;
    #endregion

    #region Properties
    public static StepManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
    }

    void Initialize()
    {
        totalStepsAmount = (int)(DataManager.Instance.currentLevel / 4) + 6;
        currentStepsAmount = 1;

        for (int i = 0; i < 2; i++)
        {
            SpawnNextStep(i, 70f);
        }
    }

    private void OnEnable()
    {
        Initialize();
    }

    public void SpawnNextStep(int amountHeightStep, float length)
    {
        if (currentStepsAmount < totalStepsAmount)
        {
            currentStepsAmount++;
            Step step = GetStepFromPool();
            step.transform.SetParent(stepsParent);
            step.SetLength(length);
            step.currentHeight = amountHeightStep;

            int maxNodeRange = 0;
            int maxObstacle = 0;
            int maxNodePos = 0;
            int minNodePos = 0;


            if (DataManager.Instance.currentLevel > 10)
            {
                if (currentStepsAmount == totalStepsAmount)
                {
                    step.ShowFinish();
                    maxNodeRange = 2;
                    maxObstacle = 5;
                    maxNodePos = 7;
                    minNodePos = 5;
                    step.canSpawnObstacles = true;
                }
                else
                if ((currentStepsAmount < totalStepsAmount) && (currentStepsAmount > (int)(totalStepsAmount * 0.7f)))
                {
                    maxNodeRange = 4;
                    maxObstacle = 8;
                    maxNodePos = 11;
                    minNodePos = 9;
                    step.canSpawnObstacles = true;

                }
                else
                {
                    maxNodeRange = 2;
                    maxObstacle = 6;
                    maxNodePos = 10;
                    minNodePos = 8;
                    step.canSpawnObstacles = false;
                }
            }
            else
            {
                if (currentStepsAmount == totalStepsAmount)
                {
                    step.ShowFinish();
                    maxNodeRange = 2;
                    maxObstacle = 5;
                    maxNodePos = 5;
                    minNodePos = 4;
                    step.canSpawnObstacles = true;
                }
                else
                if ((currentStepsAmount < totalStepsAmount) && (currentStepsAmount > (int)(totalStepsAmount * 0.6f)))
                {
                    maxNodeRange = 3;
                    maxObstacle = 6;
                    maxNodePos = 8;
                    minNodePos = 6;
                    step.canSpawnObstacles = true;

                }
                else
                {
                    maxNodeRange = 2;
                    maxObstacle = 4;
                    maxNodePos = 6;
                    minNodePos = 5;
                    step.canSpawnObstacles = false;
                }
            }

            maxNodeRange += (int)(DataManager.Instance.currentLevel / 8);
            maxNodeRange = (int)Mathf.Clamp(maxNodeRange, 1f, 4f);

            Vector3 pos = new Vector3(
                previousStep.transform.position.x,
                previousStep.transform.position.y + heightStep * amountHeightStep + Player.Instance.sizeY,
                previousStep.endPos.position.z + length * 0.5f
                );

            step.SetTMP(amountHeightStep + 1);
            step.SetPosition(pos);
            step.maxObstacle = maxObstacle;
            int randMaxPos = (int)Random.Range(minNodePos, maxNodePos);
            print(maxNodePos + " " + randMaxPos);
            step.RandomNodesPosition(randMaxPos);
            step.SpawnNode(1, maxNodeRange);

            previousStep = step;
        }
    }

    public Step GetStepFromPool()
    {
        Step instance;
        if (poolSteps == null)
        {
            poolSteps = new List<Step>();
        }
        int lastIndex = poolSteps.Count - 1;
        if (lastIndex >= 0)
        {
            instance = poolSteps[lastIndex];
            instance.gameObject.SetActive(true);
            poolSteps.RemoveAt(lastIndex);
        }
        else
        {
            instance = Instantiate(stepPrefab);
        }
        return instance;
    }

    public void ReclaimStep(Step step)
    {
        if (poolSteps == null)
        {
            poolSteps = new List<Step>();
        }
        poolSteps.Add(step);
        step.gameObject.SetActive(false);
    }
    #endregion
}

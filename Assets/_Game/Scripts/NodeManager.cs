﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    #region Editor Params
    public Node nodePrefab;
    #endregion

    #region Params
    List<Node> poolNodes = new List<Node>();
    #endregion

    #region Properties
    public static NodeManager Instance { get; private set; }
    #endregion

    #region Events
    #endregion

    #region Methods
    private void Awake()
    {
        Instance = this;
    }

    public void SpawnNode(Vector3 localPos, Transform parent)
    {
        Node instance = GetFromPool();
        instance.transform.SetParent(parent);
        instance.transform.localPosition = localPos;
    }

    public void SpawnMultipleNode(int amount, Vector3 localPos, Transform parent)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector3 tempPos = new Vector3(localPos.x, localPos.y + 1.5f * i, localPos.z + 0.55f * i);
            Node instance = GetFromPool();
            instance.transform.SetParent(parent);
            instance.transform.localPosition = tempPos;
        }
    }
    
    public Node GetFromPool()
    {
        Node instance;
        int lastIndex = poolNodes.Count - 1;
        if (lastIndex >= 0)
        {
            instance = poolNodes[lastIndex];
            instance.gameObject.SetActive(true);
            poolNodes.RemoveAt(lastIndex);
        }
        else
        {
            instance = Instantiate(nodePrefab);
        }
        return instance;
    }

    public void ReclaimNode(Node node)
    {
        poolNodes.Add(node);
        node.gameObject.SetActive(false);
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    #region Editor Params
    public Rigidbody anchor;
    public Transform model;
    public List<GameObject> listModel;
    public bool isFirstNode;
    public GameObject aura;
    public GameObject auraDead;
    #endregion

    #region Params
    FixedJoint fixedJoint;
    public GameObject currentModel;
    public Animator currentAnimator;
    public List<string> startAnim = new List<string> { "Idle", "Idle2", "Idle3", "Wave", "Falling" };
    public bool stacked;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void OnEnable()
    {
        Initialize();
    }

    public void Initialize()
    {
        stacked = false;
        RandomModel();
        currentAnimator.SetTrigger(startAnim[(int)Random.Range(0, startAnim.Count)]);
        transform.localRotation = Quaternion.identity;
        gameObject.tag = "Node";
        model.localRotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));

        fixedJoint = GetComponent<FixedJoint>();
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().WakeUp();
        ConnectBody(anchor, 0f);
    }

    public void RandomModel()
    {
        currentModel = null;
        foreach (GameObject item in listModel)
        {
            item.SetActive(false);
        }
        var m = listModel[(int)Random.Range(0, listModel.Count)];
        m.SetActive(true);
        currentModel = m;
        currentAnimator = currentModel.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            GetComponent<Rigidbody>().isKinematic = true;
            Destroy(GetComponent<FixedJoint>());
            GetComponent<Rigidbody>().Sleep();
            transform.SetParent(Player.Instance.transform.parent);
            if (!isFirstNode)
            {
                Player.Instance.StackDown(this, 20f + other.gameObject.transform.position.y);
            }
            else
            {
                Player.Instance.vertSpeed = 0f;
                GameManager.Instance.LoseGame();
            }
            DelayReclaim();
        }
        if (other.gameObject.CompareTag("Player") && !stacked)
        {
            stacked = true;
            RotateModel();
            EnableAura(aura);
        }
    }

    void DelayReclaim()
    {
        StartCoroutine(C_DelayReclaim());
    }

    IEnumerator C_DelayReclaim()
    {
        yield return new WaitForSeconds(0f);
        NodeManager.Instance.ReclaimNode(this);
    }

    public void RotateModel()
    {
        StartCoroutine(C_RotateModel());
    }

    IEnumerator C_RotateModel()
    {
        Quaternion temp = model.localRotation;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 0.35f;
            model.localRotation = Quaternion.Lerp(temp, Quaternion.Euler(Vector3.zero), t);
            yield return null;
        }
    }

    public void ConnectBody(Rigidbody rigid, float connectedMassScale)
    {
        if (fixedJoint == null)
        {
            gameObject.AddComponent<FixedJoint>();
            fixedJoint = GetComponent<FixedJoint>();
        }
        fixedJoint.connectedBody = rigid;
        fixedJoint.connectedMassScale = connectedMassScale;
    }

    public void EnableAura(GameObject aura)
    {
        StartCoroutine(C_EnableAuta(aura));
    }

    IEnumerator C_EnableAuta(GameObject aura)
    {
        aura.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        aura.SetActive(false);
    }

    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Step : MonoBehaviour
{
    #region Editor Params
    public Transform endPos;
    public Transform length;
    public Transform nodesParent;
    public TextMeshPro tmpHeight;
    public GameObject finish;
    public Obstacle obstaclePrefab;
    public Coin coinPrefabs;
    #endregion

    #region Params
    public List<Vector3> listPos = new List<Vector3>();
    public int amountNodes;
    public bool canSpawnObstacles;
    public int currentHeight;
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    public void GetAmountNodes()
    {
        amountNodes = 0;
        foreach (Transform node in nodesParent)
        {
            if (node.gameObject.activeInHierarchy)
            {
                amountNodes++;
            }
        }
    }

    public void RandomNodesPosition(int amount)
    {
        listPos.Clear();

        for (int i = 0; i < amount; i++)
        {
            Vector3 temp = new Vector3(Random.Range(-6.5f, 6.5f), 20f, i * Random.Range(5f, 6f) * 1.5f - 15f);
            listPos.Add(temp);
        }
        print("Amount: " + amount + " List: " + listPos.Count);
    }

    public int maxObstacle;
    public void SpawnNode(int minAmountNode, int maxAmountNode)
    {
        foreach (Transform item in nodesParent)
        {
            Node node = item.GetComponent<Node>();
            NodeManager.Instance.ReclaimNode(node);
        }
        if (NodeManager.Instance != null)
        {
            foreach (Vector3 localPos in listPos)
            {
                int randAmount = (int)Random.Range(minAmountNode, maxAmountNode);
                NodeManager.Instance.SpawnMultipleNode(randAmount, localPos, nodesParent);
            }

            for (int i = 0; i < (int)Random.Range(1f, maxObstacle); i++)
            {
                //if (canSpawnObstacles && (obstacleAmount <= 3))
                Obstacle instance = Instantiate<Obstacle>(obstaclePrefab);
                instance.transform.SetParent(transform);

                Vector3 pos = listPos[(int)Random.Range(1f, listPos.Count)];
                pos = new Vector3(Random.Range(-6f, 6f), pos.y + 0.5f, pos.z - 1f);
                instance.transform.localPosition = pos;

                instance.Move(Random.Range(0f, 2f));
                instance.value = (int)(Player.Instance.listNode.Count * Random.Range(0.2f, 0.4f));
                instance.value = (int)Mathf.Clamp(instance.value, 1f, 3f);
                instance.SetSize();
            }
        }
        GetAmountNodes();

        if (Random.value <= 0.2f)
        {
            SpawnCoin();
        }
    }

    List<Coin> listCoins = new List<Coin>();

    public void SpawnCoin()
    {
        listCoins.Clear();
        var randX = Random.Range(-5f, 5f);
        for (int i = 0; i < 10; i++)
        {
            Coin coinInstance = Instantiate(coinPrefabs);
            coinInstance.transform.SetParent(transform);
            coinInstance.transform.localPosition = new Vector3(randX, 50f, i * 2f);
            listCoins.Add(coinInstance);
        }
    }

    public void SetTMP(int height)
    {
        if (height <= 1)
        {
            tmpHeight.text = "";
        }
        else
        {
            tmpHeight.text = height.ToString();
        }
    }

    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }

    public void SetLength(float value)
    {
        length.localScale = new Vector3(15f, 40f, value);
    }

    [NaughtyAttributes.Button]
    public void Reclaim()
    {
        finish.SetActive(false);
        StepManager.Instance.ReclaimStep(this);
        foreach (Coin coin in listCoins)
        {
            if (coin == null)
                return;
            Destroy(coin.gameObject);
        }
    }

    public void DelayReclaim()
    {
        StartCoroutine(C_DelayReclaim());
    }

    IEnumerator C_DelayReclaim()
    {
        yield return new WaitForSeconds(0.5f);
        Reclaim();
    }

    public void ShowFinish()
    {
        finish.SetActive(true);
    }

    #endregion
}
